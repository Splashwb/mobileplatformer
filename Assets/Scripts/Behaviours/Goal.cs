﻿using UnityEngine;

public class Goal : MonoBehaviour, IInteractable
{
	public void InteractWith(BaseActor interactee)
	{
		if (interactee is Player)
		{
			// TODO:
			// Could add bonus 'points' or something else when reaching the goal running
			// Determine achievements

			GameManager.Instance.ExitCurrentLevel(true);

			// This would be the place to add achievements
		}
	}
}
