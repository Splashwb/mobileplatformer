﻿public interface IInteractable
{
	void InteractWith(BaseActor interactee);
}
