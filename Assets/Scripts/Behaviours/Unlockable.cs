﻿using UnityEngine;

public class Unlockable : MonoBehaviour, IInteractable
{
    // Editor variables
    public Key openedWith = null;

    public void InteractWith(BaseActor interactee)
    {
        if (interactee is Player && interactee.IsRunning)
        {
            bool canUnlock = InventoryManager.Instance.CheckKey(openedWith.keyType);

            if (canUnlock)
                Destroy(this.gameObject);
            else
                interactee.Stun();
        }
    }
}
