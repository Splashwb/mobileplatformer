﻿using UnityEngine;

public class TutorialPost : MonoBehaviour, IInteractable
{
    // Editor variables
    public int popupId = UIManager.NO_POPUP;

    // Bool
    private bool wasActivated = false;

    public void InteractWith(BaseActor interactee)
    {
        if (wasActivated)
            return;

        Player player = interactee.GetComponent<Player>();

        if (player != null)
        {
            UIManager.Instance.ActivatePopup(popupId);
            GameManager.Instance.PauseGame();
            wasActivated = true;
        }
    }
}
