﻿using UnityEngine;

public class Jumppad : MonoBehaviour, IInteractable
{
    // Editor Variables
    public float padJumpPower = 1.0f;
    public float reactivationTime = 0.5f;

    // Bool
    private bool isActivated = false;

    // Numbers
    private float activatedTimer = 0.0f;

    public void Update()
    {
        UpdateTimers();
    }

    private void UpdateTimers()
    {
        if (isActivated)
        {
            activatedTimer += Time.deltaTime;

            if (activatedTimer >= reactivationTime)
            {
                isActivated = false;
                activatedTimer = 0.0f;
            }
        }
    }

    public void InteractWith(BaseActor interactee)
    {
        if (interactee.CanUseJumpingPlatform && !isActivated)
        {
            interactee.Jump(padJumpPower);
            isActivated = true;
        }
    }
}
