﻿using UnityEngine;
using System.Collections.Generic;

public class InventoryManager : MonoBehaviour
{
    // Singleton
    private static InventoryManager instance;

    public static InventoryManager Instance
    {
        get { return instance; }
    }

    // Collections
    private List<Key> collectedKeys = new List<Key>();
    private List<Coin> collectedCoins = new List<Coin>();

    // Start is called before the first frame update
    void Awake()
    {
        instance = GameObject.FindObjectOfType<InventoryManager>();

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this);
        }
    }

	#region InventoryManagement

	public void AddToInventory(Collectable collectable)
    {
        switch (collectable.collectableType)
        {
            case Collectable.CollectableType.COIN:
                collectedCoins.Add((Coin)collectable);
                break;

            case Collectable.CollectableType.KEY:
                collectedKeys.Add((Key)collectable);
                break;
        }
    }

    public void ResetInventory()
    {
        collectedCoins.Clear();
        collectedKeys.Clear();
    }

    #endregion

    public bool CheckKey(Key.KeyType keyType)
    {
        foreach (Key key in collectedKeys)
        {
            if (key.keyType == keyType)
                return true;
        }

        return false;
    }

    public int CountCoinsOfType(Coin.CoinType coinType)
    {
        int coinsFound = 0;

        foreach (Coin coin in collectedCoins)
        {
            if (coin.coinType == coinType)
                coinsFound++;
        }

        return coinsFound;
    }
}
