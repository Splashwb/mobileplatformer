﻿using UnityEngine;
using UnityEngine.EventSystems;

public class UIManager : MonoBehaviour
{
    // Constants
    public const int NO_POPUP = -1;
    private const string UI_MANAGER_NAME = "UIManager";

    // Singleton
    private static UIManager instance;

    // Prefabs
    public GameObject eventSystemPrefab = null;
    public GameObject startMenuPrefab = null;
    public GameObject levelSelectPrefab = null;
    public GameObject loadingScreenPrefab = null;
    public GameObject hudPrefab = null;
    public GameObject pauseMenuPrefab = null;
    public GameObject[] popupPrefabs = null;

    // Gameobjects
    private GameObject eventSystem = null;
    private GameObject currentActiveCanvas = null;
    private GameObject startMenu = null;
    private GameObject levelSelectMenu = null;
    private GameObject loadingScreen = null;
    private GameObject hud = null;
    private GameObject pauseMenu = null;
    private GameObject[] popups = null;

    // Objects
    private LoadingScreenHandler loadingScreenHandler;

    public static UIManager Instance
    {
        get { return instance; }
    }

    public LoadingScreenHandler LoadingScreen
    {
        get { return loadingScreenHandler; }
    }

    // Start is called before the first frame update
    void Awake()
    {
        // Singleton
        if (instance == null)
        {
            instance = GameObject.FindObjectOfType<UIManager>();
            DontDestroyOnLoad(instance);
        } 
        else
        {
            Destroy(this);
            return;
        }

        InitUIManager();
    }

    private void InitUIManager()
    {
        // Ensure the event system is active
        if (EventSystem.current == null)
        {
            eventSystem = Instantiate(eventSystemPrefab);
            EventSystem.current = eventSystem.GetComponent<EventSystem>();
        } 
        else
        {
            eventSystem = GameObject.FindObjectOfType<EventSystem>().gameObject;
        }

        DontDestroyOnLoad(eventSystem);

        // Create gameobjects
        startMenu = InstantiateCanvas(startMenuPrefab);
        levelSelectMenu = InstantiateCanvas(levelSelectPrefab);
        loadingScreen = InstantiateCanvas(loadingScreenPrefab);
        hud = InstantiateCanvas(hudPrefab);
        pauseMenu = InstantiateCanvas(pauseMenuPrefab);

        // Create popups
        popups = new GameObject[popupPrefabs.Length];

        for (int i = 0; i < popupPrefabs.Length; i++)
        {
            popups[i] = InstantiateCanvas(popupPrefabs[i]);
        }

        // Assign components
        loadingScreenHandler = loadingScreen.GetComponent<LoadingScreenHandler>();
    }

    public void ActivateStartMenu()
    {
        DeActivateCurrentCanvas();
        ActivateCanvas(startMenu);
    }

    public void ActivateLevelSelectMenu()
    {
        DeActivateCurrentCanvas();
        ActivateCanvas(levelSelectMenu);
    }

    public void ActivateLoadingScreen()
    {
        DeActivateCurrentCanvas();
        ActivateCanvas(loadingScreen);
    }

    public void ActivateHud()
    {
        DeActivateCurrentCanvas();
        ActivateCanvas(hud);
    }

    public void ActivatePauseMenu()
    {
        DeActivateCurrentCanvas();
        ActivateCanvas(pauseMenu);
    }

    public void ActivatePopup(int popupId)
    {
        if (popupId <= NO_POPUP)
            return;

        DeActivateCurrentCanvas();
        ActivateCanvas(popups[popupId]);
    }

    public void DeActivateCurrentCanvas()
    {
        DeActivateCanvas(currentActiveCanvas);
    }

    private void ActivateCanvas(GameObject canvas)
    {
        if (canvas != null)
            canvas.SetActive(true);

        currentActiveCanvas = canvas;
    }

    private void DeActivateCanvas(GameObject canvas)
    {
        if (canvas != null)
            canvas.SetActive(false);

        currentActiveCanvas = null;
    }

    private GameObject InstantiateCanvas(GameObject prefab)
    {
        GameObject newCanvas = null;

        newCanvas = Instantiate(prefab);
        DeActivateCanvas(newCanvas);
        DontDestroyOnLoad(newCanvas);
        
        return newCanvas;
    }
}
