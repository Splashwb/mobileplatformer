﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Enum
    private enum GameState
    {
        NONE = -1,
        MENU = 0,
        LOADING = 1,
        GAME = 2,
    }

    // Global constants
    public const int PRIMARY_TOUCH = 0;

    // Constants
    private const int NO_LEVEL = -1;
    private const string PLAYER_PREFS_LEVEL_KEY = "Level_";

    // Editor variables
    public const string START_SCENE = "MainScene";
    public const string LOADING_SCENE = "LoadingScene";
    public string[] levelScenes = null;
    public float forceShowLoadingScreenTime = 1.5f;
    public UIManager uiManagerPrefab = null;

    // Singleton
    private static GameManager instance;

    // Numbers
    private GameState currentGameState = GameState.NONE;
    private int currentLevelId = NO_LEVEL;

    // String
    private string levelSceneToLoad = string.Empty;

    // Bool
    private bool isPaused = false;

    public static GameManager Instance
    {
        get { return instance; }
    }

    public bool IsPaused
    {
        get { return isPaused; }
    }

    public int TotalAmountOfLevels
    {
        get { return levelScenes.Length; }
    }

    public void Awake()
    {
        if (instance == null)
        {
            instance = GameObject.FindObjectOfType<GameManager>();
            DontDestroyOnLoad(instance);
        } 
        else
        {
            Destroy(this);
            return;
        }

        // Instantiate Managers
        Instantiate(uiManagerPrefab);

        // Hook up scene event listeners
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

	#region PlayerPrefs

    // TODO; move this to a player prefs manager if more is needed, else it gets to cluttered.
	private void SetCurrentLevelCompleted()
    {
        PlayerPrefs.SetInt(PLAYER_PREFS_LEVEL_KEY + currentLevelId, 1);
    }

    public bool IsLevelUnlocked(int levelId)
    {
        if (levelId == 0)
            return true;

        return GetLevelCompletedFor(levelId - 1);
    }

    public bool GetLevelCompletedFor(int levelId)
    {
        if (levelId <= NO_LEVEL)
            return false;

        bool hasKey = PlayerPrefs.HasKey(PLAYER_PREFS_LEVEL_KEY + levelId);
        bool completed = false;

        if (hasKey)
            completed = PlayerPrefs.GetInt(PLAYER_PREFS_LEVEL_KEY + levelId) == 1;

        return completed;
    }

    #endregion

    #region GameManagement

    public void PauseGame()
    {
        isPaused = true;
    }

    public void UnPauseGame()
    {
        UIManager.Instance.ActivateHud();
        isPaused = false;
    }

    public void StartLevel(int levelId)
    {
        currentLevelId = levelId;
        levelSceneToLoad = levelScenes[currentLevelId];

        LoadNewLevel(LOADING_SCENE);
    }

    public void ExitCurrentLevel(bool completedLevel)
    {
        if (completedLevel)
            SetCurrentLevelCompleted();

        LoadStartScene();
    }

    #endregion

    #region SceneManagement

    public void LoadNewLevel(string sceneName)
    {
        SceneManager.LoadScene(sceneName);

        StartCoroutine(LoadSceneAsync());
    }

    public void RestartCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void LoadStartScene()
    {
        SceneManager.LoadScene(START_SCENE);
    }

    public void OnSceneLoaded(Scene loadedScene, LoadSceneMode loadSceneMode)
    {
        if (loadedScene.name.Equals(START_SCENE))
        {
            // Check if coming back from the game.
            if (currentGameState <= GameState.MENU)
                UIManager.Instance.ActivateStartMenu();
            else
                UIManager.Instance.ActivateLevelSelectMenu();

            currentLevelId = NO_LEVEL;
            currentGameState = GameState.MENU;
        } 
        else if (loadedScene.name.Equals(LOADING_SCENE))
        {
            UIManager.Instance.ActivateLoadingScreen();

            currentGameState = GameState.LOADING;
        } 
        else
        {
            UIManager.Instance.ActivateHud();

            currentGameState = GameState.GAME;
        }
    }

    private IEnumerator LoadSceneAsync()
    {
        float showLoadingScreenTime = 0.0f;

        // Add a little waiting time, could be used to show tips or ads
        UIManager.Instance.LoadingScreen.SetLoadingScreenWaiting();

        while (showLoadingScreenTime < forceShowLoadingScreenTime)
        {
            showLoadingScreenTime += Time.deltaTime;
            UIManager.Instance.LoadingScreen.SetProgress(showLoadingScreenTime / forceShowLoadingScreenTime);

            yield return new WaitForEndOfFrame();
        }

        UIManager.Instance.LoadingScreen.SetLoadingScreenLoading();

        // Start the operation.
        AsyncOperation sceneLoadOperation = SceneManager.LoadSceneAsync(levelSceneToLoad, LoadSceneMode.Single);

        while (sceneLoadOperation.progress < 1)
        {
            UIManager.Instance.LoadingScreen.SetProgress(sceneLoadOperation.progress);
            yield return new WaitForEndOfFrame();
        }

        UIManager.Instance.LoadingScreen.SetProgress(1);
        levelSceneToLoad = string.Empty;
    }

    #endregion
}
