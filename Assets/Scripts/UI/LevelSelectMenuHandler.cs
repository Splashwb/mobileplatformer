﻿using UnityEngine;
using UnityEngine.UI;

public class LevelSelectMenuHandler : MonoBehaviour
{
	// Editor variables
	public GameObject[] levelButtons = null;
	public Sprite levelLockedImage = null;
	public Sprite levelUnlockedImage = null;
	public Sprite levelCompletedImage = null;

	public void OnEnable()
	{
		DetermineCurrentLevelsState();
	}

	public void PressedLevelButton(int levelId)
	{
		if (GameManager.Instance.IsLevelUnlocked(levelId))
			GameManager.Instance.StartLevel(levelId);
	}

	private void DetermineCurrentLevelsState()
	{
		for (int i = 0; i < GameManager.Instance.TotalAmountOfLevels; i++)
		{
			// First determine if this level is completed.
			bool completedLevel = GameManager.Instance.GetLevelCompletedFor(i);

			if (completedLevel)
			{
				// Completed level!
				levelButtons[i].GetComponent<Image>().sprite = levelCompletedImage;
			}
			else
			{
				// Not completed level, but is it unlocked?
				levelButtons[i].GetComponent<Image>().sprite = GameManager.Instance.IsLevelUnlocked(i) ? levelUnlockedImage : levelLockedImage;
			}
		}
	} 
}
