﻿using UnityEngine;

public class HUDHandler : MonoBehaviour
{
	public void PauseGame()
	{
		UIManager.Instance.ActivatePauseMenu();
		GameManager.Instance.PauseGame();
	}
}
