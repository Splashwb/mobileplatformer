﻿using UnityEngine;

public class PopupHandler : MonoBehaviour
{
	public void DismissPopup()
	{
		GameManager.Instance.UnPauseGame();
	}
}
