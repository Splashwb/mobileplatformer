﻿using UnityEngine;
using UnityEngine.UI;

public class PauseMenuHandler : MonoBehaviour
{
	// Constants
	private const string COLLECTED_COINS_PREFIX = "X ";

	// Editor variables
	public GameObject coinsCollectedText = null;
	public GameObject coinsCollectedShadowText = null;

	public void Update()
	{
		UpdateCoinsCollected();
	}

	private void UpdateCoinsCollected()
	{
		int amountOfCoinsCollected = InventoryManager.Instance.CountCoinsOfType(Coin.CoinType.GOLD);

		coinsCollectedText.GetComponent<Text>().text = COLLECTED_COINS_PREFIX + amountOfCoinsCollected.ToString();
	}

	public void UnPauseGame()
	{
		GameManager.Instance.UnPauseGame();
	}

	public void ExitGame()
	{
		GameManager.Instance.ExitCurrentLevel(false);
	}
}
