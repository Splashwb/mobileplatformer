﻿using UnityEngine;

public class StartMenuHandler : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        UpdateInput();
    }

    private void UpdateInput()
    {
        if (Input.GetMouseButtonDown(GameManager.PRIMARY_TOUCH)) // TODO, grab constant from game manager, add constant to game manager
            UIManager.Instance.ActivateLevelSelectMenu();
    }
}
