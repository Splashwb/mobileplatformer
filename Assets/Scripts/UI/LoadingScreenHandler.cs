﻿using UnityEngine;
using UnityEngine.UI;

public class LoadingScreenHandler : MonoBehaviour
{
    // Consts
    private const string LOADING_TEXT = "Loading";
    private const string WAITING_TEXT = "Waiting";

    // Editor variables
    public Image progressBar;
    public Text progressText;
    public Text progressTextShadow;

    public void SetLoadingScreenWaiting()
    {
        progressTextShadow.text = WAITING_TEXT;
        progressText.text = WAITING_TEXT;
        progressBar.fillAmount = 0.0f;
    }

    public void SetLoadingScreenLoading()
    {
        progressTextShadow.text = LOADING_TEXT;
        progressText.text = LOADING_TEXT;
        progressBar.fillAmount = 0.0f;
    }

    public void SetProgress(float progress)
    {
        progressBar.fillAmount = progress;
    }
}
