﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Editor variables
    public GameObject background;
    public GameObject player;
    public float cameraMinY = 0.0f;

    // Objects
    private DeviceOrientation currentOrientation;

    void Awake()
    {
        if (player == null)
            Debug.Log("CameraController.Awake(): No player gameobject assigned!");
        else
            FocusOnPlayer();

        if (background == null)
            Debug.Log("CameraController.Awake(): No background gameobject assigned!");
        else
            AdjustBackgroundSize();  // Set initial background size and position

#if UNITY_ANDROID
        currentOrientation = Input.deviceOrientation;
#endif
    }

    // Update is called once per frame
    void Update()
    {
        FocusOnPlayer();

#if UNITY_ANDROID
        DetectOrientationChange();
#endif
    }

    // TODO: Should be called when changing aspect from/to landscape/portrait, must be an event somewhere.
    private void AdjustBackgroundSize()
    {
        if (background == null)
            return;

        // Get components
        SpriteRenderer backgroundSpriteRenderer = background.GetComponent<SpriteRenderer>();

        // Setup variables
        float cameraHeight = Camera.main.orthographicSize * 2;
        Vector2 cameraSize = new Vector2(Camera.main.aspect * cameraHeight, cameraHeight);
        Vector2 spriteSize = backgroundSpriteRenderer.sprite.bounds.size;
        Vector2 backgroundScale = Vector2.one;

        // Determine landscape/portrait and adjust
        backgroundScale.x = cameraSize.x / spriteSize.x;
        backgroundScale.y = cameraSize.y / spriteSize.y;

        // Set to background
        background.transform.localScale = backgroundScale;
    }

    private void FocusOnPlayer()
    {
        // Do not let the camera follow the player when falling down, which means the player is dead
        float cameraY = Mathf.Max(cameraMinY, player.transform.position.y);

        this.transform.position = new Vector3(player.transform.position.x, cameraY, -10);
        background.transform.position = new Vector2(this.transform.position.x, this.transform.position.y);
    }

    private void DetectOrientationChange()
    {
        if (currentOrientation != Input.deviceOrientation)
            AdjustBackgroundSize();
    }
}
