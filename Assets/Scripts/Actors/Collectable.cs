﻿using UnityEngine;

public class Collectable : MonoBehaviour
{
    public enum CollectableType
    {
        COIN = 0,
        KEY = 1,
    }

    // Editor variables
    public CollectableType collectableType = CollectableType.COIN;
    public float maxFloatingAmplitude = 0.1f;
    public float floatingSpeed = 1.0f;

    // Numbers
    private float currentMovement;

    // Vector2
    private Vector2 startPos = Vector2.zero;

    // Bool
    private bool moveUp = true;

    public void Start()
    {
        startPos = this.transform.position;
    }

    public void Collect()
    {
        InventoryManager.Instance.AddToInventory(this);
        Destroy(this.gameObject);
    }

    public void Update()
    {
        FloatCollectable();
    }

    private void FloatCollectable()
    {
        // Go up and down.
        if (maxFloatingAmplitude != 0)
        {
            if (moveUp)
                currentMovement += floatingSpeed * Time.deltaTime;
            else
                currentMovement -= floatingSpeed * Time.deltaTime;

            this.transform.position = new Vector2(startPos.x, startPos.y + currentMovement);

            if (currentMovement >= maxFloatingAmplitude)
                moveUp = false;
            else if (currentMovement <= -maxFloatingAmplitude)
                moveUp = true;
        }
    }
}
