﻿using UnityEngine;

public class BaseActor : MonoBehaviour
{
    // Consts
    public const string LAYER_ENVIRONMENT = "Environment";
    public const string LAYER_ACTOR = "Actors";
    public const string LAYER_INTERACTABLES = "Interactables";
    
    public enum ActorFacing
    {
        FACING_LEFT = -1,
        FACING_RIGHT = 1,
    }

    // Editor variables
    public bool canUseJumpingPlatform = true;
    public int hitpoints = 1;
    public bool canBeStunned = false;
    public float maxStunTime = 0.0f;

    // Components
    private Rigidbody2D rigidBody;
    private Collider2D collider;

    // Numbers
    protected ActorFacing currentFacing = ActorFacing.FACING_RIGHT;
    protected float currentMoveSpeed = 0.0f;
    protected int currentHitPoints = 0;
    private float currentStunTime = 0.0f;

    // Vector
    private Vector2 currentVelocity = Vector2.zero;

    // Bool
    protected bool isGrounded = true;
    protected bool isRunning = false;
    protected bool hasCollidedMidAir = false;
    protected bool isStunned = false;

    public bool CanUseJumpingPlatform
    {
        get { return canUseJumpingPlatform; }
    }

    public bool IsRunning
    {
        get { return isRunning; }
    }

    public virtual void Awake()
    {
        rigidBody = this.gameObject.GetComponent<Rigidbody2D>();
        collider = this.gameObject.GetComponent<Collider2D>();

        currentHitPoints = hitpoints;
    }

    public virtual void Update()
    {
        if (GameManager.Instance.IsPaused)
        {
            currentVelocity = Vector2.zero;
            currentMoveSpeed = 0;
            return;
        }

        CheckIsGrounded();

        if (hasCollidedMidAir && isGrounded)
            hasCollidedMidAir = false;

        UpdateStunnedTimer();
    }

    public void UpdateStunnedTimer()
    {
        if (isStunned)
        {
            currentStunTime += Time.deltaTime;

            if (currentStunTime >= maxStunTime)
            {
                currentStunTime = 0.0f;
                isStunned = false;
            }
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer(LAYER_INTERACTABLES))
        {
            // Collided a trigger that is interactable
            IInteractable interactable = collision.gameObject.GetComponent<IInteractable>();

            if (interactable != null)
                interactable.InteractWith(this);
        }
    }

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer(LAYER_ENVIRONMENT))
        {
            // Collided with environment
            if (collision.GetContact(0).normal.x != 0 && !isGrounded)
            {
                // Resolving collisions mid air so any actor will not stick to the wall.
                hasCollidedMidAir = true;
                currentMoveSpeed = 0;
            }
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer(LAYER_INTERACTABLES))
        {
            // Collided with an interactable
            IInteractable interactable = collision.gameObject.GetComponent<IInteractable>();

            if (interactable != null)
                interactable.InteractWith(this);
        }
    }

    #region Physics

    public void CheckIsGrounded()
    {
        RaycastHit2D raycastHit = Physics2D.Raycast((Vector2)this.gameObject.transform.position + collider.offset, Vector2.down, collider.bounds.size.y / 2, LayerMask.GetMask(LAYER_ENVIRONMENT));
        isGrounded = raycastHit.collider != null;
    }

    public virtual void FixedUpdate()
    {
        if (GameManager.Instance.IsPaused)
            return;

        UpdateVelocity();
    }

    private void UpdateVelocity()
    {
        if (rigidBody != null)
        {
            currentVelocity.x = currentMoveSpeed;
            currentVelocity.y = rigidBody.velocity.y;

            rigidBody.velocity = currentVelocity;
        }
    }

    #endregion

    public void Jump(float jumpPower)
    {
        if (rigidBody != null)
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpPower);
    }

    public virtual void Hit()
    {
        if (isStunned)
            return;

        currentHitPoints--;

        if (currentHitPoints <= 0)
            Die();
        else
            Stun();
    }

    public virtual void Stun()
    {
        // TODO:
        // Override in player
        // Add basic stun effect
        if (canBeStunned)
            isStunned = true;
    }

    protected virtual void Die()
    {
        Destroy(this.gameObject);
    }
}
