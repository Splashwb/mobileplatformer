﻿using UnityEngine;

public class Coin : Collectable
{
	public enum CoinType
	{
		BRONZE = 0,
		SILVER = 1,
		GOLD = 2,
	}

	public CoinType coinType = CoinType.BRONZE;
}
