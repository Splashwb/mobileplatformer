﻿using UnityEngine;

public class Key : Collectable
{
	public enum KeyType
	{
		GREEN = 0,
		BLUE = 1,
		BRONZE = 2,
		GOLD = 3,
	}

	public KeyType keyType;
}
