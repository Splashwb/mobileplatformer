﻿using UnityEngine;

public class BaseEnemy : BaseActor
{
	// Editor variables
	public float acceleration = 0.2f;
	public float maxMoveSpeed = 2.5f;
	public float maxMovementDistance = 10.0f;
	public ActorFacing startFacing = ActorFacing.FACING_LEFT;
	public bool canJump = false;
	public float jumpingPower = 10.0f;

	// Numbers
	private float totalMovement = 0.0f;

	// Components
	private SpriteRenderer sprite;

	public void Start()
	{
		// Assign values
		currentFacing = startFacing;

		// Get components
		sprite = this.gameObject.GetComponent<SpriteRenderer>();
	}

	public override void OnCollisionEnter2D(Collision2D collision)
	{
		base.OnCollisionEnter2D(collision);

		if (collision.gameObject.layer == LayerMask.NameToLayer(LAYER_ACTOR))
		{
			// Collided with an actor
			BaseActor actor = collision.gameObject.GetComponent<BaseActor>();

			if (actor != null)
			{
				if (!actor.IsRunning)
					ReverseMovement();
			}
		}
	}

	public override void Update()
	{
		base.Update();

		if (GameManager.Instance.IsPaused)
			return;

		UpdateAnimation();
	}

	public override void FixedUpdate()
	{
		base.FixedUpdate();

		if (GameManager.Instance.IsPaused)
			return;

		UpdateMovement();
	}

	private void UpdateMovement()
	{
		// Increase speed.
		currentMoveSpeed += (int)currentFacing * acceleration;

		// Clamp for max speed.
		currentMoveSpeed = Mathf.Clamp(currentMoveSpeed, -maxMoveSpeed, maxMoveSpeed);

		// Keep track of total movement done
		totalMovement += Mathf.Abs(currentMoveSpeed);

		if (totalMovement >= maxMovementDistance)
		{
			ReverseMovement();

			// Do a jump if i'm allowed.
			if (canJump)
				Jump(jumpingPower);
		}
	}

	private void ReverseMovement()
	{
		// Reverse move direction
		totalMovement = 0;
		currentMoveSpeed = 0;
		currentFacing = currentFacing == ActorFacing.FACING_LEFT ? ActorFacing.FACING_RIGHT : ActorFacing.FACING_LEFT;
	}

	private void UpdateAnimation()
	{
		sprite.flipX = currentFacing == ActorFacing.FACING_RIGHT;
	}
}
