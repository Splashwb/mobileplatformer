﻿using UnityEngine;

public class Player : BaseActor
{
    // Constants
    private const float MAX_TIME_BETWEEN_TAPS = 0.75f;
   
    // Animations
    private const string ANIM_IDLE = "Idle";
    private const string ANIM_WALKING = "Walking";
    private const string ANIM_RUNNING = "Running";

    // Editor variables
    public float acceleration = 0.2f;
    public float maxMoveSpeed = 3.5f;
    public float maxRunSpeed = 7.0f;
    public Sprite[] playerSprites = null;
    public Animator animator = null;

    // Components
    private SpriteRenderer sprite;

    // Numbers
    private float doubleTapTimer = 0.0f;

    // Bool
    private bool tappedScreen;

    public override void Awake()
    {
        base.Awake();

        sprite = this.GetComponent<SpriteRenderer>();
        animator = this.GetComponent<Animator>();
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        base.OnTriggerEnter2D(collision);

        // Check for collectables
        Collectable collectable = collision.gameObject.GetComponent<Collectable>();

        if (collectable != null)
            collectable.Collect();
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        
        if (collision.gameObject.layer == LayerMask.NameToLayer(LAYER_ACTOR))
        {
            // Collided with an actor
            BaseActor actor = collision.gameObject.GetComponent<BaseActor>();

            if (actor != null)
            {
                if (isRunning)
                    actor.Hit();
                else
                    Hit();
            }
        }
    }

    #region Update

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        if (GameManager.Instance.IsPaused)
            return;

        UpdateInput();
        UpdateAnimation();
        UpdatePlayerInsideCamera();
    }

    private void UpdateInput()
    {
        // Update double tap timer
        if (tappedScreen && !isRunning)
        {
            doubleTapTimer += Time.deltaTime;

            if (doubleTapTimer >= MAX_TIME_BETWEEN_TAPS)
            {
                // Reset run detection
                doubleTapTimer = 0.0f;
                tappedScreen = false;
            }
        }

        // Pressing tap 
        if (Input.GetMouseButtonDown(GameManager.PRIMARY_TOUCH))
        {
            if (tappedScreen)
            {
                // First check if the correct side of the screen was tapped again
                ActorFacing tapFacing = GetFacingFromTouchPosition(Input.mousePosition);

                if (currentFacing == tapFacing)
                {
                    // Double tap detected -> Enable run
                    isRunning = true; 
                    tappedScreen = false;
                }

                doubleTapTimer = 0.0f;
            } 
            else
            {
                tappedScreen = true;
            }
        }

        // Holding tap
        if (Input.GetMouseButton(GameManager.PRIMARY_TOUCH))
        {
            // Check new facing
            ActorFacing lastFacing = currentFacing;
            currentFacing = GetFacingFromTouchPosition(Input.mousePosition);

            // Reset current speed to avoid a slow transition in speed when switching move direction
            if (lastFacing != currentFacing)
                currentMoveSpeed = 0;

            // Increase current speed by acceleration
            if (!hasCollidedMidAir)
                currentMoveSpeed += (int)currentFacing * acceleration;

            // Limit speed.
            if (isRunning)
                currentMoveSpeed = Mathf.Clamp(currentMoveSpeed, -maxRunSpeed, maxRunSpeed);
            else
                currentMoveSpeed = Mathf.Clamp(currentMoveSpeed, -maxMoveSpeed, maxMoveSpeed);
        } 
        
        // Releasing tap
        if (Input.GetMouseButtonUp(GameManager.PRIMARY_TOUCH))
        {
            currentMoveSpeed = 0;
            isRunning = false;
        }
    }

    private void UpdateAnimation()
    {
        sprite.flipX = currentFacing == ActorFacing.FACING_LEFT;

        if (Mathf.Abs(currentMoveSpeed) > 0)
        {
            if (isRunning)
                SetAnimationActive(ANIM_RUNNING);
            else
                SetAnimationActive(ANIM_WALKING);
        } 
        else
        {
            SetAnimationActive(ANIM_IDLE);
        }

        // TODO:
        // Add animation for being stunned -> Just flash the sprite red or some particle effect?
        // Add jumping animation
    }

    private void UpdatePlayerInsideCamera()
    {
        Vector2 viewportPoint = Camera.main.WorldToViewportPoint(this.transform.position);

        if (viewportPoint.y < 0)
            Die();
    }

    #endregion

    #region Player

    protected override void Die()
    {
        // TODO; should show popup with try again etc.
        GameManager.Instance.ExitCurrentLevel(false);
    }

    #endregion

    #region Animation

    private void SetAnimationActive(string newAnimation)
    {
        int hash = Animator.StringToHash(newAnimation);

        if (animator.GetCurrentAnimatorStateInfo(0).shortNameHash != hash && !newAnimation.Equals(string.Empty))
            animator.Play(newAnimation);
    }

    #endregion

    #region Helpers

    private ActorFacing GetFacingFromTouchPosition(Vector2 touchPosition)
    {
        Vector2 touchPos = Camera.main.ScreenToViewportPoint(Input.mousePosition);

        // Touchpos.x range is between 0 and 1
        if (touchPos.x < 0.5f)
            return ActorFacing.FACING_LEFT;
        else
            return ActorFacing.FACING_RIGHT;
    }

    #endregion
}
